package controller;

import model.Student;
import model.Subject;
import view.DAOMVC;
import view.SubjectMVC;

public class SubjectController implements SubjectMVC.Controller {

    private DAOMVC.Controller daoController;
    private SubjectMVC.View view;

    public SubjectController(DAOMVC.Controller daoController) {
        this.daoController = daoController;
    }

    @Override
    public void attach(SubjectMVC.View view) {
        this.view = view;
    }

    @Override
    public void addSubject(String name) {
        daoController.getData().getSubjectDAO().addSubject(name);
    }

    @Override
    public void addStudentToSubject(Subject subject, Student student) {
        if (daoController.getData().getSubjectDAO().getID(student, subject) == 0) {
            daoController.getData().getSubjectDAO().addStudentToSubject(subject, student);
        } else {
            System.out.println("Error message");
        }
    }

    @Override
    public String printSubject() {
        return daoController.getData().getSubjectDAO().printSubject();
    }

    @Override
    public Subject getSubjectBuID(int ID) {
        return daoController.getData().getSubjectDAO().getSubjectByID(ID);
    }

    public int getStudentToSubjectID(Subject subject, Student student) {
        return daoController.getData().getSubjectDAO().getID(student, subject);
    }
}
