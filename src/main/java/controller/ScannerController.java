package controller;

import view.ScannerMVC;

import java.util.Scanner;

public class ScannerController implements ScannerMVC.Controller {
    @Override
    public String getName() {
        return new Scanner(System.in).nextLine();
    }

    @Override
    public int getID() {
        return new Scanner(System.in).nextInt();
    }
}
