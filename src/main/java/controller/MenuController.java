package controller;

import view.MenuMVC;

public class MenuController implements MenuMVC.Controller {

    MenuMVC.View view;

    @Override
    public String initMessage() {
        String initMessage = "Witaj w naszej szkole wybierz co chcesz zrobić: " +
                "+ \n + 1. +\\t + Dodaj ucznia + \\n " +
                "+ 2. Dodaj przedmiot do ucznia + \\n " +
                "+ 3.+\\t+Dodaj ocene uczniowi do przedmiotu " +
                "+ \\n+4.Popraw uczniowi ocene +\\n+ " +
                "5.+\\t+Zakończ rok szkolny";
        return initMessage;
    }

    @Override
    public void attach(MenuMVC.View view) {
        this.view = view;

    }

    @Override
    public void start() {
        view.showInitMessage();

    }

    @Override
    public void option(int value) {
        switch (value) {
            case 1: {
                view.addStudent();
            }
            break;
            case 2: {
                view.studentToSubject();
            }
            break;
            case 3: {
                view.addGrade();
            }
            break;
            case 4: {
                view.correctGrade();
            }
            break;
            case 5: {
                view.finish();
            }
            break;
            case 6: {
                view.returnMenu();
            }
            default: {
                view.returnMenu();
            }
        }
    }
}
