package controller;

import model.SubjectToStudent;
import view.DAOMVC;
import view.GradeMVC;

import java.util.List;

public class GradeController implements GradeMVC.Controller {

    DAOMVC.Controller daoController;
    GradeMVC.View view;

    public GradeController(DAOMVC.Controller daoController) {
        this.daoController = daoController;
    }

    @Override
    public void attach(GradeMVC.View view) {
        this.view = view;

    }

    @Override
    public void correctGrade(SubjectToStudent subjectToStudent, int correctGrade) {
        daoController.getData().getGradeDAO().correctGrade(subjectToStudent, correctGrade);

    }

    @Override
    public void addGrade(SubjectToStudent subjectToStudent, int grade) {
        daoController.getData().getGradeDAO().addGrade(subjectToStudent, grade);

    }

    @Override
    public int getGradesID(SubjectToStudent subjectToStudent) {
        return daoController.getData().getGradeDAO().getGradesID(subjectToStudent);
    }

    @Override
    public double calculateAverageForSubject(int ID) {
        return daoController.getData().getGradeDAO().calculateAverageForSubject(ID);
    }

    @Override
    public double calculateAverageForStudent(List<Integer> ID) {
        double average = 0.0;
        average = ID.stream()
                .mapToDouble(this::calculateAverageForSubject)
                .sum();
        return average / ID.size();
    }
}
