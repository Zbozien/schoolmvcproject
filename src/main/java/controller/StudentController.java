package controller;

import model.Student;
import view.DAOMVC;
import view.StudentsMVC;

public class StudentController implements StudentsMVC.Controller {

    private DAOMVC.Controller daoController;
    private StudentsMVC.View view;

    public StudentController(DAOMVC.Controller daoController) {
        this.daoController = daoController;
    }

    @Override
    public void attach(StudentsMVC.View view) {
        this.view = view;
    }

    @Override
    public void addStudent(String name) {
        Student student = new Student();
        student.setName(name);
        daoController.getData().getStudentDAO().addStudent(student);

    }

    @Override
    public void removeStudent(Student student) {
        daoController.removeStudent(student);

    }

    @Override
    public String printStudents() {
        return daoController.getData().getStudentDAO().showListOfStudents();
    }

    @Override
    public Student getStudentByID(int ID) {
        return daoController.getData().getStudentDAO().getStudentByID(ID);
    }
}
