package controller;

import model.DataBase;
import model.Student;
import view.DAOMVC;

public class DAOController implements DAOMVC.Controller {

    private DataBase dataBase;

    public DAOController() {
        dataBase = DataBase.getInstance();
    }

    @Override
    public DataBase getData() {
        return dataBase;
    }

    @Override
    public void removeStudent(Student student) {
        if (dataBase.getStudentDAO().checkStudent(student)) {
            dataBase.getStudentDAO().removeStudent(student);
            // usuniecie z bazy przedmiotów
            // usuniecie z bazy ocen
        }

    }
}
