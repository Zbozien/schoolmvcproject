package model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

// logika dodawania ocen
public class GradeDAO {

    private List<Grade> grades = new ArrayList<>();

    public void addGrade(SubjectToStudent subjectToStudent, int gradeValue) {

        Grade grade = new Grade();
        if (grades.isEmpty()) {
            grade.setId(1);
        } else {
            grade.setId(grades.get(grades.size() - 1).getId() + 1);
        }
        grade.setGradeValue(gradeValue);
        grade.setSubjectToStudentID(subjectToStudent.getId());
        grades.add(grade);
    }

    public void correctGrade(SubjectToStudent subjectToStudent, int correctGrade) {

        List<Grade> tmp = grades.stream()
                .filter(a -> a.getSubjectToStudentID() == subjectToStudent.getId())
                .collect(Collectors.toList());

        if (!tmp.isEmpty() && tmp.get(0).getCorrectGrade() != -1) {
            tmp.get(0).setCorrectGrade(correctGrade);
        } else {
            System.out.println("error message");
        }
    }

    public int getGradesID(SubjectToStudent subjectToStudent) {
        List<Grade> tmp = grades.stream()
                .filter(a -> a.getSubjectToStudentID() == subjectToStudent.getId())
                .collect(Collectors.toList());

        if (tmp.isEmpty()) {
            return 0;
        } else {
            return tmp.get(0).getId();
        }
    }

    public double calculateAverageForSubject(int ID) {

        double average = 0.0;

        List<Grade> tmp = grades.stream()
                .filter(a -> a.getId() == ID)
                .collect(Collectors.toList());

        average = tmp.get(0).getGradeValue();
        if (!(tmp.get(0).getCorrectGrade() == -1)) {
            average += tmp.get(0).getCorrectGrade();
            average /= 2;
        }
        return average;
    }
}
