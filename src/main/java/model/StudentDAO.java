package model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentDAO {

    private List<Student> students = new ArrayList<>();

    public void addStudent(Student student) {
        if (students.isEmpty()) {
            student.setId(1);
        } else {
            student.setId((students.get(students.size() - 1)).getId() + 1);
        }
        students.add(student);
    }

    public void removeStudent(Student studentToRemove) {
        int id = studentToRemove.getId();
        students = students.stream()
                .filter(student -> student.getId() != id)
                .collect(Collectors.toList());
    }

    public String showListOfStudents() {
        String studentList = "";
        studentList = students.stream()
                .map(a -> a.getId() + "\t" + a.getName() + "\n")
                .toString();
        return studentList;
    }

    public boolean checkStudent(Student student) {
        long isExist = 0;
        isExist = students.stream()
                .filter(a -> a.getId() == student.getId())
                .count();
        if (isExist >= 1) {
            return true;
        } else {
            return false;
        }
    }

    public Student getStudentByID(int ID) {
        Student student = new Student();
        List<Student> tmpList = students.stream()
                .filter(a -> a.getId() == ID)
                .collect(Collectors.toList());
        student = tmpList.get(0);

        return student;
    }
}
