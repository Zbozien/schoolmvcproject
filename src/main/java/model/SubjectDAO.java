package model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SubjectDAO {

    private List<Subject> subjects = new ArrayList<>();
    private List<SubjectToStudent> subjectToStudentsList = new ArrayList<>();

    public void addSubject(String name) {
        Subject subject = new Subject();
        if (subjects.isEmpty()) {
            subject.setId(1);
        } else {
            subject.setId(subjects.get(subjects.size() - 1).getId() + 1);
        }
        subject.setName(name);
    }

    public String printSubject() {
        String subjectList = "";
        subjectList = subjects.stream()
                .map(a -> a.getId() + "\t" + a.getName() + "\n")
                .toString();
        return subjectList;
    }

    public Subject getSubjectByID(int ID) {
        Subject subject = new Subject();
        List<Subject> tmpList = subjects.stream()
                .filter(a -> a.getId() == ID)
                .collect(Collectors.toList());
        subject = tmpList.get(0);

        return subject;
    }

    public void addStudentToSubject(Subject subject, Student student) {

        int subjectId = subject.getId();
        int studentId = student.getId();
        SubjectToStudent subjectToStudent = new SubjectToStudent();

        if (subjectToStudentsList.isEmpty()) {
            subjectToStudent.setId(1);
        } else {
            subjectToStudent.setId(subjectToStudentsList.get(subjectToStudentsList.size() - 1).getId() + 1);
        }
        subjectToStudent.setId(subjectId);
        subjectToStudent.setId(studentId);
        subjectToStudentsList.add(subjectToStudent);
    }

    public int getID(Student student, Subject subject) {
        int studentID = student.getId();
        int subjectID = subject.getId();

        List<SubjectToStudent> tempList =
                subjectToStudentsList.stream()
                        .filter(a -> ((a.getStudentId() == studentID) && (a.getSubjectId() == subjectID)))
                        .collect(Collectors.toList());

        if (tempList.size() == 0) {
            return 0;
        } else {
            return tempList.get(0).getId();
        }
    }


}
