package model;

public class Grade {

    private int id; // id oceny
    private int subjectToStudentID; //przypisanie do kazdego studenta osobnego przedmiotu
    private int gradeValue; // ocena 1 - 6
    private int correctGrade = -1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubjectToStudentID() {
        return subjectToStudentID;
    }

    public void setSubjectToStudentID(int subjectToStudentID) {
        this.subjectToStudentID = subjectToStudentID;
    }

    public int getGradeValue() {
        return gradeValue;
    }

    public void setGradeValue(int gradeValue) {
        this.gradeValue = gradeValue;
    }

    public int getCorrectGrade() {
        return correctGrade;
    }

    public void setCorrectGrade(int correctGrade) {
        this.correctGrade = correctGrade;
    }
}
