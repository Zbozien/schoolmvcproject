package model;
//singleton pattern
public class DataBase {

    private static DataBase instance;
    private SubjectDAO subjectDAO = new SubjectDAO();
    private GradeDAO gradeDAO = new GradeDAO();
    private StudentDAO studentDAO = new StudentDAO();

    private DataBase() {

    }

    public static DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase();
        }
        return instance;
    }

    public SubjectDAO getSubjectDAO() {
        return subjectDAO;
    }

    public GradeDAO getGradeDAO() {
        return gradeDAO;
    }

    public StudentDAO getStudentDAO() {
        return studentDAO;
    }
}
