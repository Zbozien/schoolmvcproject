package view;

public interface ScannerMVC {

    interface Controller {
        String getName();

        int getID();
    }

    interface View {

    }
}
