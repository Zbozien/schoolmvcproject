package view;

import model.SubjectToStudent;

import java.util.List;

public interface GradeMVC {

    interface Controller {
        void attach(View view);

        void correctGrade(SubjectToStudent subjectToStudent, int correctGrade);

        void addGrade(SubjectToStudent subjectToStudent, int grade);

        int getGradesID(SubjectToStudent subjectToStudent);

        double calculateAverageForSubject(int ID);

        double calculateAverageForStudent(List<Integer> ID);
    }

    interface View {

    }
}
