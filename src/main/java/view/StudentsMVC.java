package view;

import model.Student;

public interface StudentsMVC {

    interface Controller {
        void attach(View view);

        void addStudent(String name);

        void removeStudent(Student student);

        String printStudents();

        Student getStudentByID(int ID);
    }

    interface View {

    }
}
