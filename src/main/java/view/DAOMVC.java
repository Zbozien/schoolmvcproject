package view;

import model.DataBase;
import model.Student;

public interface DAOMVC {

    interface Controller {

        DataBase getData();

        void removeStudent(Student student);
    }

    interface View {

    }
}
