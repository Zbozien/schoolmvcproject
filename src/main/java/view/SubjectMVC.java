package view;

import model.Student;
import model.Subject;

public interface SubjectMVC {

    interface Controller {

        void addSubject(String name);

        void addStudentToSubject(Subject subject, Student student);

        String printSubject();

        Subject getSubjectBuID(int ID);

        void attach(SubjectMVC.View view);
    }

    interface View {

    }
}
