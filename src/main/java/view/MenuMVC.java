package view;

public interface MenuMVC {

    interface Controller {

        String initMessage();

        void attach(View view);

        void start();

        void option(int value);
    }

    interface View {

        void showInitMessage();

        void addStudent();

        void studentToSubject();

        void addGrade();

        void correctGrade();

        void finish();

        void returnMenu();

    }
}
