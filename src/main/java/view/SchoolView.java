package view;

import controller.*;

public class SchoolView implements StudentsMVC.View, SubjectMVC.View, GradeMVC.View, MenuMVC.View {

    StudentsMVC.Controller students;
    SubjectMVC.Controller subject;
    DAOMVC.Controller dao;
    ScannerMVC.Controller scanner;
    GradeMVC.Controller grade;
    MenuMVC.Controller menu;

    public SchoolView() {
        dao = new DAOController();
        students = new StudentController(dao);
        subject = new SubjectController(dao);
        grade = new GradeController(dao);
        scanner = new ScannerController();
        menu = new MenuController();

        students.attach(this);
        subject.attach(this);
        grade.attach(this);
        menu.attach(this);

        subject.addSubject("Matematyka ");
        subject.addSubject("Fizyka ");
        subject.addSubject("Jezyk polski ");
        subject.addSubject("W-F ");
        subject.addSubject("Jezyk Angielski ");
        subject.addSubject("Historia ");

        menu.start();

    }


    @Override
    public void studentToSubject() {
        System.out.println(students.printStudents());
        int studentID = scanner.getID();
        System.out.println(subject.printSubject());
        int subjectID = scanner.getID();

        subject.addStudentToSubject(subject.getSubjectBuID(subjectID), students.getStudentByID(studentID));
        returnMenu();
    }

    @Override
    public void returnMenu() {
        showInitMessage();

    }

    @Override
    public void addStudent() {
        students.addStudent(scanner.getName());
        returnMenu();

    }

    public void removeStudent() {
        System.out.println(students.printStudents());
        students.removeStudent(students.getStudentByID(scanner.getID()));
        returnMenu();

    }

    @Override
    public void correctGrade() {

    }

    @Override
    public void addGrade() {

    }

    @Override
    public void showInitMessage() {
        System.out.println(menu.initMessage());
        int option = scanner.getID();

    }

    @Override
    public void finish() {

    }
}
